package com.example.rouletteapp;

public class Product
{
    private String name, charityName, price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCharityName() {
        return charityName;
    }

    public void setCharityName(String charityName) {
        this.charityName = charityName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Product(String name, String charityName, String price) {
        this.name = name;
        this.charityName = charityName;
        this.price = price;
    }
}
